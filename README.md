# resnet50-docker
This repository contains code and Docker image to deploy a pretrained object detection model in keras.
This pretrained object detection model recognizes 1000 different classes of objects in the ImageNet 2012 Large Scale Visual Recognition Challenge. 
The model consists of a deep convolutional net using the [ResNet-50](https://keras.io/examples/cifar10_resnet/) architecture that was trained on the ImageNet-2012 data set. 
The input to the model is a 224x224 image, and the output is a list of estimated class probilities.

The model is based on the Keras built-in model for ResNet-50. The code in this repository deploys the model as a web service in a Docker container. 
This repository was developed as part of the Future Facts session with the dutch railway (NS) data-engineering team.


